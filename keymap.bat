REM -------------------------------------------------------------------------------------------------------------------
REM This is a script for remapping keys to make the Mac keyboard suck less.
REM -------------------------------------------------------------------------------------------------------------------

@ECHO OFF

REM We remap the following keys:
REM   - CapsLock:Escape (0x14:0x1b)
REM   - LeftMeta:LeftCtrl (0x5b:0xa2)
REM   - LeftAlt:LeftMeta (0xa4:0x5b)
REM   - LeftCtrl:LeftAlt (0xa2:0xa4)
REM   - RightMeta:RightCtrl (0x5c:0xa3)
REM   - RightAlt:RightMeta (0xa5:0x5c)
C:\windows\uncap.exe 0x14:0x1b 0x5b:0xa2 0xa4:0x5b 0xa2:0xa4 0x5c:0xa3 0xa5:0x5c

REM -------------------------------------------------------------------------------------------------------------------