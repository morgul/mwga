# Make Windows Great Again

This is a set of scripts to help map Windows keys and functionality to the same functionality I'm used to in macOS. See, it's not that I feel macOS is the be-all-end-all, it's just that I have nearly a decade of muscle memory. I need that to map 1:1 when I switch OSes.

## `keymap.bat`

_Note: This isn't used in favor of the AutoHotKey solution!_

This is a little batch script that remaps modifier keys. It uses a little application called [uncap][], that it uses to do this.

[uncap]: https://github.com/susam/uncap

## `MakeWindowsGreatAgain.ahk`

This is an [AutoHotKey][ahk]

[ahk]: 