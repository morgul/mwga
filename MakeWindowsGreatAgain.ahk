﻿; ---------------------------------------------------------------------------------------------------------------------
; MakeWindowsGreatAgain
; 
;
; The whole point of this script is to make Windows behave more like macOS, from a keymapping standpoint. I have years 
; of muscle memory on the keyboard, and I shouldn't need to re-lean it on the rare occasions I need/want to use windows.
; ---------------------------------------------------------------------------------------------------------------------

; AutoHotKey version: 1.1.30.03

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; ---------------------------------------------------------------------------------------------------------------------
; Remap Hotkey Shortcuts
; ---------------------------------------------------------------------------------------------------------------------

; Remap Windows Desktop Switching
>#Up::
    Send {Blind}^#{Right}
    return
>#Down::
    Send {Blind}^#{Left}
    return

; ---------------------------------------------------------------------------------------------------------------------
; Basic Key Remaps
; ---------------------------------------------------------------------------------------------------------------------

; The Touchbar macs are missing an ESC key, so we use CapsLock.
Capslock::
    Send {ESC}
    return

; Remap the left modifier keys
LWin::LCtrl
LCtrl::LAlt
LAlt::LWin

; Remap the right modifier keys
RWin::RCtrl
RAlt::RWin